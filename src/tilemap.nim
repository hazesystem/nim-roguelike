import nim_tiled

type
  Tiles* {.pure.} = enum
    floor = 1, wall, door, locked_door, secret_door = 17

var
  tilebuffer*: seq[int]
  tiledMapWidth*: int
  tiledMapHeight*: int
  tileWidth*: int
  tileHeight*: int

  tiledMap: Map
  tileset*: TIleset

proc loadMap*() =
  tiledMap = loadTiledMap("res/roguelike.tmx").orDefault
  tileset = tiledMap.tilesets[0]

  tilebuffer = tiledMap.layers[0].data.tiles
  tiledMapWidth = tiledMap.layers[0].width
  tiledMapHeight = tiledMap.layers[0].height
  tileWidth = tileset.tilewidth
  tileHeight = tileset.tileheight
