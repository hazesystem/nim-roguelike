import std/[math, random], items, tilemap, util

type
  MobKind = enum kMob, kPlayer
  Mob* = object of RootObj
    x*, y*: int
    width*, height*: int = 8
    hp*: int = 1
    maxHp*: int = 1
    power*: int = 1
    damaged* = false
    dead* = false
    playerAction* = false
    seekingPlayer* = false
    playerLastSeen*: array[2, int]

    case kind: MobKind
    of kPlayer: inventory*: seq[Item]
    else: discard

var mobs*: seq[Mob]
var player* = Mob(x: 2, y: 13, hp: 8, maxHp: 8, kind: kPlayer)

proc search_inventory(find_item: ItemKind): bool =
  result = false
  for i, item in player.inventory:
    if item.kind == find_item:
      player.inventory.delete i
      return true

proc hurt*(m: var Mob, dmg: int) =
  let hp = m.hp - dmg

  if hp > 0:
    m.hp = hp
  else:
    m.hp = 0
    m.dead = true

proc attack*(m: var Mob) =
  if m.kind == kMob:
    player.hurt m.power

proc move*(m: var Mob, dir: array[2, int]) =
  let
    nx = m.x + dir[0]
    ny = m.y + dir[1]

  for mob in mobs.mitems:
    if [mob.x, mob.y] == [nx, ny] and not mob.dead:
      if m.kind == kPlayer:
        mob.hurt m.power
        return
    elif [player.x, player.y] == [nx, ny]:
      m.attack
      return

  if m.kind == kPlayer:
    var idx = -1
    for i, item in items.items:
      if [item.x, item.y] == [nx, ny]:
        m.inventory.add item
        idx = i
    if idx > -1:
      items.items.delete idx
      idx = -1

  case tileAt(nx, ny):
  of ord(Tiles.wall): discard
  of ord(Tiles.door):
    if m.kind == kPlayer:
      tilebuffer[nx + ny * tiledMapWidth] = ord(Tiles.floor)
  of ord(Tiles.locked_door):
    if m.kind == kPlayer:
      if search_inventory ItemKind.Key:
        tilebuffer[nx + ny * tiledMapWidth] = ord(Tiles.floor)
  of ord(Tiles.secret_door):
    if m.kind == kPlayer:
      if search_inventory ItemKind.SecretKey:
        tilebuffer[nx + ny * tiledMapWidth] = ord(Tiles.floor)
  else:
    m.x = nx
    m.y = ny

proc seek*(m: var Mob, x, y: int) =
  let dist = [x - m.x, y - m.y]
  let dir_norm = normalize(dist[0], dist[1])
  var dir = [0, 0]

  if dir_norm[0] > 0:
    dir[0] = (ceil(dir_norm[0])).int
  else:
    dir[0] = (floor(dir_norm[0])).int

  if dir_norm[1] > 0:
    dir[1] = (ceil(dir_norm[1])).int
  else:
    dir[1] = (floor(dir_norm[1])).int

  let
    ax = abs(dist[0])
    ay = abs(dist[1])

  if ax > ay:
    if not tileAt(m.x + dir[0], m.y).checkSolid:
      let d = [dir[0], 0]
      m.move d
    else:
      let d = [0, dir[1]]
      m.move d

  elif ay > ax:
    if not tileAt(m.x, m.y + dir[1]).checkSolid:
      let d = [0, dir[1]]
      m.move d
    else:
      let d = [dir[0], 0]
      m.move d

  elif [m.x, m.y] == m.playerLastSeen:
    m.seekingPlayer = false

  else:
    var rand_dir = rand(3)
    if (rand_dir == 0):
      m.move [0, dir[1]]

    else:
      m.move [dir[0], 0]

proc update*(m: var Mob, dir: array[2, int], action: bool) =
  m.playerAction = false
  if dir != [0, 0]:
    m.move dir
    m.playerAction = true
  elif action:
    m.playerAction = true

proc update*(m: var Mob) =
  let
    ray = castRay(player.x, player.y, m.x, m.y)
    sightBlocked = ray[0]
    dist = ray[1]
  # echo sightBlocked, ", ", dist

  if player.playerAction:
    if sightBlocked and not m.seekingPlayer:
      if rand(1..3) == 1:
        case rand(3)
        of 0: m.move Direction.Up
        of 1: m.move Direction.Down
        of 2: m.move Direction.Left
        of 3: m.move Direction.Right
        else: discard

    elif sightBlocked and m.seekingPlayer:
      m.seek(m.playerLastSeen[0], m.playerLastSeen[1])

    elif not sightBlocked and dist <= 50.0:
      m.playerLastSeen = [player.x, player.y]
      m.seekingPlayer = true
      m.seek(m.playerLastSeen[0], m.playerLastSeen[1])
