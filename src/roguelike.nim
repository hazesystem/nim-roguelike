import std/[options, os, random], nim_tiled, sdl2, sdl2/[image, ttf]
import items, mobs, tilemap, util

const
  offset = 80
  TileMultiplier = 24
  TextWidth = 32
  TextHeight = 16

type
  Sprites {.pure.} = enum
    player = 128, enemy, key = 144, secret_key

var
  window: WindowPtr
  render: RendererPtr

proc main =
  sdl2.init(INIT_VIDEO or INIT_TIMER or INIT_EVENTS)
  discard image.init(IMG_INIT_PNG)
  ttf.ttfInit()
  defer: ttf.ttfQuit()
  defer: image.quit()
  defer: sdl2.quit()

  randomize()
  loadMap()

  window = createWindow("Roguelike", SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED, (tiledMapWidth * TileMultiplier).cint,
    (tiledMapHeight * TileMultiplier + offset).cint, SDL_WINDOW_SHOWN)
  render = createRenderer(window, -1, Renderer_Accelerated or
      Renderer_PresentVsync or Renderer_TargetTexture)
  defer: destroy render
  defer: destroy window

  render.setScale(TileMultiplier / tileWidth, TileMultiplier / tileHeight)

  let
    tilesetImage = render.loadTexture "res".joinPath(tileset.image.get().source).cstring
    spritesheetImage = render.loadTexture "res/rogue-sprites.png"

  let font = ttf.openFont("res/Silkscreen-Regular.ttf", TextHeight)

  var
    event = sdl2.defaultEvent
    runGame = true

  # Create mobs and items
  mobs.mobs.add Mob(x: 6, y: 8)
  items.items.add Item(x: 3, y: 13, kind: Key)
  items.items.add Item(x: 3, y: 11, kind: SecretKey)

  while runGame:
    var
      dir = [0, 0]
      action = false

    while pollEvent event:
      case event.kind
      of QuitEvent:
        runGame = false
        break
      of KeyDown:
        case event.key.keysym.scancode
        of SDL_SCANCODE_UP, SDL_SCANCODE_W: dir = Direction.Up
        of SDL_SCANCODE_DOWN, SDL_SCANCODE_S: dir = Direction.Down
        of SDL_SCANCODE_LEFT, SDL_SCANCODE_A: dir = Direction.Left
        of SDL_SCANCODE_RIGHT, SDL_SCANCODE_D: dir = Direction.Right
        of SDL_SCANCODE_Z: action = true
        of SDL_SCANCODE_ESCAPE: runGame = false
        else: discard
      else: discard

    render.setDrawColor 64, 64, 64
    render.clear

    # Render map and UI
    for i, tile in tilebuffer:
      let
        srcrect = rect(((tile - 1) * tileWidth).cint, 0, tileWidth.cint,
            tileHeight.cint)
        dstrect = rect(((i mod tiledMapWidth) * tileWidth).cint, ((
            i div tiledMapWidth) * tileHeight + offset div 3).cint,
            tileWidth.cint,
            tileHeight.cint)
      render.copy(tilesetImage, addr srcrect, addr dstrect)

    let
      color = color(255, 255, 0, 0)
      text = "HP " & $player.hp & "/" & $player.maxHp
      surface = ttf.renderTextSolid(font, text.cstring, color)
      texture = render.createTextureFromSurface(surface)

    surface.freeSurface
    defer: texture.destroy

    let r = rect(0, -(TextHeight/4).cint, TextWidth, TextHeight)
    render.copy texture, nil, addr r

    # draw inventory
    for i, item in player.inventory:
      var sprite = 0
      case item.kind
      of Key:
        sprite = ord(Sprites.key)
      of SecretKey:
        sprite = ord(Sprites.secret_key)
      of HealthPotion:
        discard

      let
        srcrect = rect((1 * (sprite * item.width)).cint, 0,
            item.width.cint, item.height.cint)
        dstrect = rect((50 + (i * 8)).cint, 0, item.width.cint,
            item.height.cint)
      render.copy(spritesheetImage, addr srcrect, addr dstrect)

    # Render and update player
    if player.dead:
      runGame = false
      break
    player.update(dir, action)
    let
      srcrect = rect((1 * (ord(Sprites.player) * player.width)).cint, 0,
          player.width.cint, player.height.cint)
      dstrect = rect((player.x * player.width).cint, (player.y *
          player.height + offset div 3).cint, player.width.cint,
          player.height.cint)
    render.copy(spritesheetImage, addr srcrect, addr dstrect)

    # Render items
    for item in items.items:
      var sprite = 0
      case item.kind
      of Key:
        sprite = ord(Sprites.key)
      of SecretKey:
        sprite = ord(Sprites.secret_key)
      of HealthPotion:
        discard

      let
        srcrect = rect((1 * (sprite * item.width)).cint, 0,
            item.width.cint, item.height.cint)
        dstrect = rect((item.x * item.width).cint, (item.y * item.height +
            offset div 3).cint, item.width.cint, item.height.cint)
      render.copy(spritesheetImage, addr srcrect, addr dstrect)

    # Render and update mobs
    for m in mobs.mobs.mitems:
      if m.dead:
        break

      m.update()
      let
        srcrect = rect((1 * (ord(Sprites.enemy) * m.width)).cint, 0,
            m.width.cint, m.height.cint)
        dstrect = rect((m.x * m.width).cint, (m.y * m.height +
            offset div 3).cint, m.width.cint, m.height.cint)
      render.copy(spritesheetImage, addr srcrect, addr dstrect)
    render.present
main()
