import std/math, tilemap

const
  Direction* = (
    Up: [0, -1],
    Down: [0, 1],
    Left: [-1, 0],
    Right: [1, 0]
  )

proc tileAt*(x, y: int): int =
  result = tilebuffer[x + y * tiledMapWidth]

proc checkSolid*(t: int): bool =
  result = t in [ord(Tiles.wall), ord(Tiles.door), ord(Tiles.locked_door), ord(
      Tiles.secret_door)]

func normalize*(x, y: int): array[2, float] =
  let magnitude = sqrt((x * x + y * y).float)
  result = [x.float / magnitude, y.float / magnitude]

proc castRay*(playerx, playery, enemyx, enemyy: int): tuple =
  let
    px = playerx * tileWidth + tileWidth div 2
    py = playery * tileHeight + tileHeight div 2
    ex = enemyx * tileWidth + tileWidth div 2
    ey = enemyy * tileHeight + tileHeight div 2

    rayStart = [px, py]
    diff = [ex - rayStart[0], ey - rayStart[1]]
    distBetween = sqrt((diff[0] ^ 2 + diff[1] ^ 2).float)
    rayDir = normalize(diff[0], diff[1])

    rayUnitStepSize = [
      sqrt(1 + ((rayDir[1] / rayDir[0]) ^ 2)),
      sqrt(1 + ((rayDir[0] / rayDir[1]) ^ 2))
    ]
  var
    mapCheck = rayStart
    rayLength = [0.0, 0.0]
    step = [0, 0]

  if rayDir[0] < 0:
    step[0] = -1
    rayLength[0] = -rayUnitStepSize[0]
  else:
    step[0] = 1
    rayLength[0] = rayUnitStepSize[0]

  if rayDir[1] < 0:
    step[1] = -1
    rayLength[1] = -rayUnitStepSize[1]
  else:
    step[1] = 1
    rayLength[1] = rayUnitStepSize[1]

  let maxDist = 100.0
  var
    tileFound = false
    dist = 0.0

  while tileFound == false and dist < maxDist:
    if rayLength[0] < rayLength[1]:
      mapCheck[0] = mapCheck[0] + step[0]
      dist = rayLength[0]
      rayLength[0] = rayLength[0] + rayUnitStepSize[0]
    else:
      mapCheck[1] = mapCheck[1] + step[1]
      dist = rayLength[1]
      rayLength[1] = rayLength[1] + rayUnitStepSize[1]

    if dist <= distBetween:
      let
        x = floor(mapCheck[0] / 8).int
        y = floor(mapCheck[1] / 8).int
      tileFound = tileAt(x, y).checkSolid

  result = (tileFound, distBetween)
