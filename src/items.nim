type
  ItemKind* = enum
    Key, SecretKey, HealthPotion

  Item* = object of RootObj
    x*, y*: int
    width*, height*: int = 8
    kind*: ItemKind

var items*: seq[Item]
