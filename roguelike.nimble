# Package

version       = "0.1.0"
author        = "Haze System"
description   = "Very simple traditional roguelike"
license       = "MIT"
srcDir        = "src"
bin           = @["roguelike"]


# Dependencies

requires "nim >= 2.0.0", "fusion", "nim_tiled", "necsus#head", "sdl2", "vmath"